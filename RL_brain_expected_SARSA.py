import numpy as np
import pandas as pd


class rlExpectedSarsa:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "Expected SARSA"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self, observation):
        self.check_state_exist(observation)

        # Using epsilon greedy policy choose to explore or exploit
        if np.random.uniform() < self.epsilon:
            # Choosing to exploit

            # In Expected SARSA learning we choose the max q-table value from the available actions
            state_action = self.q_table.loc[observation, :]

            # choose next max q. pick random for equal values
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            # Choosing to explore
            action = np.random.choice(self.actions)
        return action

    '''Update the Q(S,A) state-action value table using the latest experience
       The formula used is Q(s, a) += α [ reward + (γ * Σ_a p(Q(s_, a))Q(s_, a)) - Q(s, a) ] 
    '''
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            # a_ = np.argmax(np.array(self.q_table.loc[s_, :]))
            # use formula stated above. Since the probabilities are equal => mean(Q(s_, :))
            # mean = np.mean(self.q_table.loc[s_, :])
            expected_value = 0
            for action in self.actions:
                if a_ == action:
                    policy = self.epsilon / len(self.actions) + 1 - self.epsilon
                else:
                    policy = self.epsilon / len(self.actions)
                expected_value += policy * self.q_table.loc[s_, a_]

            q_target = self.lr * (r + (self.gamma * expected_value) - self.q_table.loc[s, a])
        else:
            # This is terminal! No need to choose
            q_target = self.lr * (r - self.q_table.loc[s, a])
        self.q_table.loc[s, a] += q_target  # update
        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
